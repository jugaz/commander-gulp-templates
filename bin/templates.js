#!/usr/bin/env node

'use strict';

var
    gulp = require('gulp'),
    pug = require('gulp-pug'),
    mkdirp = require('mkdirp'),
    program = require('commander'),
    rimraf = require('rimraf')

;

/* ######################## OPTIONS ######################## */
var options = {};


/* ######################## VERSION ######################## */
program
    .version(
        'gulp-templates-cli version: ' + require('../package.json').version + '\n' +
        'pug version: ' + require('gulp-pug/package.json').version
    )
    .option('-m, --mkdirp <path>', 'create folder', createFolder)
    .option('-r, --rimraf <path>', 'delete folder', deleteFolder)


/* ######################## CREATE FOLDERS ######################## */
function createFolder(dir) {
    mkdirp(dir, function (err) {
        if (err) {
            console.error(err)
        } else {
            console.log(dir)
        }
    })
}


/* ######################## DELETE FOLDERS ######################## */
function deleteFolder(dir) {
    rimraf(dir, function (err) {
        if (err) {
            console.error(err)
        } else {
            console.log(dir)
        }
    })
}


/* ######################## GULP TEMPLATES ######################## */
// example node index.js templates 'templates/**/*.pug'  --odir 'build/'
program
    .command('templates <dir>')
    .option("--t [options]")
    .action((input, options) => {
        var input = options.input || options.parent.rawArgs[3]
        var ouput = options.ouput || options.parent.rawArgs[5]
        return gulp.src(input)
            .pipe(pug())
            .pipe(gulp.dest(ouput));
    })

program.parse(process.argv);